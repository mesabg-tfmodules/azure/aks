resource "random_string" "this" {
  length  = 12
  lower   = true
  numeric = false
  special = false
  upper   = false
}

resource "azurerm_user_assigned_identity" "this" {
  name                = "aks-${var.name}-${var.environment}"
  location            = data.azurerm_resource_group.this.location
  resource_group_name = data.azurerm_resource_group.this.name

  tags                = local.tags
}

resource "azurerm_role_assignment" "this" {
  scope                = data.azurerm_resource_group.this.id
  role_definition_name = "Network Contributor"
  principal_id         = azurerm_user_assigned_identity.this.principal_id
}

resource "azurerm_kubernetes_cluster" "this" {
  name                      = "aks-${var.name}-${var.environment}"
  location                  = data.azurerm_resource_group.this.location
  resource_group_name       = data.azurerm_resource_group.this.name
  dns_prefix                = "${var.name}-${var.environment}"

  kubernetes_version        = var.kubernetes_version
  automatic_upgrade_channel = "stable"
  private_cluster_enabled   = false
  node_resource_group       = var.kubernetes_node_resource_group
  sku_tier                  = var.kubernetes_sku_tier
  oidc_issuer_enabled       = true
  workload_identity_enabled = true

  # Under development
  # api_server_access_profile {
  #   vnet_integration_enabled = true
  #   subnet_id                = var.public_subnet_id
  # }

  network_profile {
    network_plugin = "azure"
    dns_service_ip = var.kubernetes_dns_service_ip
    service_cidr   = var.kubernetes_service_cidr
  }

  default_node_pool {
    name                        = "general"
    vm_size                     = var.kubernetes_node_pool_vm_size
    vnet_subnet_id              = var.public_subnet_id
    orchestrator_version        = var.kubernetes_version
    type                        = "VirtualMachineScaleSets"
    auto_scaling_enabled        = true
    node_count                  = var.kubernetes_node_pool_min_count
    min_count                   = var.kubernetes_node_pool_min_count
    max_count                   = var.kubernetes_node_pool_max_count
    os_disk_size_gb             = var.kubernetes_node_pool_os_disk_size_gb
    os_disk_type                = var.kubernetes_node_pool_os_disk_type
    os_sku                      = var.kubernetes_node_pool_os_sku
    temporary_name_for_rotation = random_string.this.result

    node_labels = {
      role = "general"
    }
  }

  identity {
    type         = "UserAssigned"
    identity_ids = [azurerm_user_assigned_identity.this.id]
  }

  tags = local.tags

  lifecycle {
    ignore_changes = [default_node_pool[0].node_count]
  }

  depends_on = [
    azurerm_role_assignment.this,
  ]
}
