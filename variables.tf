variable "name" {
  type        = string
  description = "Project name"
}

variable "environment" {
  type        = string
  description = "Environment name"
  default     = "develop"
}

variable "resource_group_name" {
  type        = string
  description = "Resource group name"
}

variable "kubernetes_version" {
  type        = string
  description = "Kubernetes version"
  default     = "1.29"  
}

variable "kubernetes_node_resource_group" {
  type        = string
  description = "Kubernetes node resource group name" 
}

variable "kubernetes_sku_tier" {
  type        = string
  description = "Kubernetes SKU tier"
  default     = "Standard"  
}

variable "kubernetes_node_pool_vm_size" {
  type        = string
  description = "Kubernetes node pool VM size"
  default     = "Standard_DS2_v2"
}

variable "kubernetes_node_pool_min_count" {
  type        = number
  description = "Kubernetes node pool min count"
  default     = 1
}

variable "kubernetes_node_pool_max_count" {
  type        = number
  description = "Kubernetes node pool max count"
  default     = 10
}

variable "kubernetes_node_pool_os_disk_size_gb" {
  type        = number
  description = "Kubernetes node pool instance disk size"
  default     = 64
}

variable "kubernetes_node_pool_os_disk_type" {
  type        = string
  description = "Kubernetes node pool instance disk type"
  default     = "Managed"
}

variable "kubernetes_node_pool_os_sku" {
  type        = string
  description = "Kubernetes node pool instance os sku"
  default     = "AzureLinux"
}

variable "kubernetes_dns_service_ip" {
  type        = string
  description = "Kubernetes DNS service IP"
  default     = "10.0.64.10"  
}

variable "kubernetes_service_cidr" {
  type        = string
  description = "Kubernetes service CIDR"
  default     = "10.0.64.0/19"  
}

variable "public_subnet_id" {
  type        = string
  description = "Public subnet identifier"
}

variable "private_subnet_id" {
  type        = string
  description = "Private subnet identifier"
}

variable "tags" {
  type        = map
  description = "Additional default tags"
  default     = {}
}
