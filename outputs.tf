output "id" {
  value       = azurerm_kubernetes_cluster.this.id
  description = "Azure Kubernetes Cluster identifier"
}

output "name" {
  value       = azurerm_kubernetes_cluster.this.name
  description = "Azure Kubernetes Cluster name"
}

output "kube_config" {
  value       = azurerm_kubernetes_cluster.this.kube_config
  description = "Azure Kubernetes Cluster kube config"
  sensitive   = true
}

output "kubelet_identity" {
  value       = azurerm_kubernetes_cluster.this.kubelet_identity
  description = "Azure Kubernetes Cluster kubelet identity"
}
